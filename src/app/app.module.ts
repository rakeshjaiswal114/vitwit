import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RoletypeComponent } from './roletype/roletype.component';
import { InformationComponent } from './information/information.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main/main.component';
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: '',
    component: MainComponent,
    children: [
  { path: 'role', component: RoletypeComponent },
  { path: 'info', component: InformationComponent },
  { path: 'dashboard', component: DashboardComponent },
    ]
  },
  
  
  { path: '**', component: DashboardComponent },
  ];

@NgModule({
  declarations: [
    AppComponent,
    RoletypeComponent,
    InformationComponent,
    DashboardComponent,
    HeaderComponent,
    SidebarComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
