import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  toggleSidebar (){
    var bodyElement = document.getElementsByTagName("body"); 
    bodyElement[0].classList.toggle("hideSidebar");
  }

}
